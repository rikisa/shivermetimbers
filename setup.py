"""Best Discordbot on Kuebels Server"""

from setuptools import setup, find_packages
import versioneer


setup(
    name='ShiverMeTimbers',
    # version='1.0.0',
    url='https://github.com/mypackage.git',
    author='Rikisa',
    # author_email='author@gmail.com',
    description='Best Discordbot on Kuebels Server',
    packages=find_packages(),
    # install_requires=['numpy >= 1.11.1', 'matplotlib >= 1.5.1'],

    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),

)
