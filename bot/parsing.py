import re
from typing import List
import discord
from datetime import datetime, timezone as tz, timedelta as td
import pytz


class SessionTime:
    """Like Planned time, but just time
    """

    _weekdays_short = {
        0: 'Mo.',
        1: 'Di.',
        2: 'Mi.',
        3: 'Do.',
        4: 'Fr.',
        5: 'Sa.',
        6: 'So.'}

    _weekdays_long = {
        0: 'Montag',
        1: 'Dienstag',
        2: 'Mittwoch',
        3: 'Donnerstag',
        4: 'Freitag',
        5: 'Samstag',
        6: 'Sonntag'}

    _full_pat = re.compile(
        '^(?P<pre>.*?)' +
        '(?:(?:' + '|'.join(list(_weekdays_long.values()) +
                            list(_weekdays_short.values())) + '), +)?' +
        '(?P<day>\d{1,2})\.(?P<mon>\d{1,2})\.?' +
        '(?:(?: *, *| *| *\| *| +um +| +ab +)?(?P<hh>\d{1,2}):(?P<mm>\d{1,2}))?'
        + '(?P<post>.*?)$',
        re.IGNORECASE)

    def __init__(self, day, mon, hh=None, mm=None, pre=None, post=None):
        # now = datetime.now(tz=tz(td(hours=1)))

        tz = pytz.timezone('Europe/Berlin')
        now = tz.localize(datetime.now())
        self.datetime = now.replace(day=day, month=mon, hour=0, minute=0,
                                    second=0, microsecond=0)
        if hh is not None and mm is not None:
            hh, mm = [int(v) for v in (hh, mm)]
            self.datetime = self.datetime.replace(hour=hh, minute=mm)
            self.hh = hh
            self.mm = mm
        else:
            self.hh = self.mm = None

        self.day = day
        self.mon = mon

        self.pre = pre
        self.post = post

    @property
    def weekday_long(self):
        return self._weekdays_long[self.datetime.weekday()]

    @property
    def weekday_short(self):
        return self._weekdays_short[self.datetime.weekday()]

    def __str__(self):
        ret = f'{self.weekday_long}, {self.day:0>2}.{self.mon:0>2}.'
        if self.hh is not None:
            ret += f' um {self.hh:0>2}:{self.mm:0>2} Uhr'
        return ret

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if isinstance(other, SessionTime):
            return self.datetime == other.datetime
        else:
            return self.datetime == other

    def __lt__(self, other):
        if isinstance(other, SessionTime):
            return self.datetime < other.datetime
        else:
            return self.datetime < other

    def __gt__(self, other):
        if isinstance(other, SessionTime):
            return self.datetime > other.datetime
        else:
            return self.datetime > other

    def __hash__(self):
        return hash(self.datetime)

    @classmethod
    def from_str(cls, content):
        match = cls._full_pat.match(content)
        if match is None:
            return None
        values = match.groupdict()
        for k in ('day', 'mon', 'hh', 'mm'):
            v = values.get(k)
            if v is not None:
                v = int(v)
            values[k] = v
        values.setdefault('pre', '')
        values.setdefault('post', '')
        try:
            return cls(**values)
        except ValueError:
            return None

class Announcement:

    _date_symbol = ':point_right:'
    _reminder_symbol = ':up:'
    # _count_symbol = ':white_check_mark:'
    _count_symbol = '\u2705'
    _out_symbol = '\u274c'
    _maybe_symbol = '\u274c'
    _pre_count = ' (bisher **dabei: '
    _post_count = ' **)'
    _mention_symbol = '\u25B7'

    _pat = re.compile(
        '^.*?\u2500{5} (?P<name>.*) \u2500{5}' +
        '\n\n' + _date_symbol.replace(':', '\:') + ' (?P<date>.*?)\n\n'
        '(?P<body>.*?)\n' + _mention_symbol +'(?P<mentions>.*?)\n.*$',
        re.IGNORECASE ^ re.DOTALL)

    # _pat_count = re.compile(
    #     f'^(?P<pre>.*?)' + _count_symbol.replace(':', '\:') +
    #     '.*?\d{1,3}(?P<post>.*?)$', re.IGNORECASE)

    _pat_count = re.compile(
        f'^(?P<pre>.*?)' + '\u2705' +
        '.*?\d{1,3}(?P<post>.*?)$', re.IGNORECASE)


    def __init__(self, name, date, body, mentions, msg=None):
        self.name, self.body, self.date = name, body, date
        self.mentions, self.message = set(mentions), msg

    @classmethod
    def _parse_content(cls, content):
        match = cls._pat.match(content)
        if match is None:
            return None
        name, date, body, mentions = match.groups()
        parsed = []
        for row in body.split('\n'):
            cnt = cls._pat_count.match(row)
            if cnt is not None:
                pre, post = cnt.groups()
                post = post.replace(cls._post_count, '')
                row = pre + cls._count_symbol + post
            parsed.append(row)
        body = '\n'.join(parsed)

        name = name.strip('*')
        date = SessionTime.from_str(date)
        mentions = [mnt for mnt in mentions.strip(' ,\n').split(' ') if mnt != '']
        return name, date, body, mentions

    async def parse_reactions(self, ignored: List[str] = []):
        """Parse reactions from message
        """
        opt_ins = set([])
        opt_outs = set([])

        for reaction in self.message.reactions:
            if reaction.emoji == self._count_symbol:
                cur_set = opt_ins
            elif reaction.emoji == self._out_symbol:
                cur_set = opt_outs
            else:
                continue
            users = await reaction.users().flatten()
            for user in users:
                if user.mention in ignored:
                    continue
                cur_set.add(user.mention)

        for mentee in opt_ins.difference(opt_outs):
            self.add_player(mentee)

    def to_markdown(self):
        line = '\u2500' * 5

        the_time = f'{self.date.weekday_long}'
        the_time += f' {self.date.day:0>2}.{self.date.mon:0>2}.'
        if self.date.hh is not None:
            the_time += f' um {self.date.hh:0>2}:{self.date.mm:0>2} Uhr'

        cnt_relp = (f'{self._count_symbol}{self._pre_count}{len(self.mentions)}'
                    + f'{self._post_count}')
        sub_body = self.body.replace(self._count_symbol, cnt_relp)

        md = f'_ _\n{line} **{self.name}** {line}'
        md += f'\n\n{self._date_symbol} *{the_time}*\n\n'
        md += sub_body
        md += f'\n{self._mention_symbol}{" ".join(self.mentions)}'
        md += f'\n\n_ _'

        return md

    def add_player(self, mention):
        self.mentions.add(mention)

    def remove_player(self, mention):
        if mention in self.mentions:
            self.mentions.remove(mention)

    @classmethod
    def from_msg(cls, message: discord.Message):
        values = cls._parse_content(message.content)
        if values is None:
            return None
        values += (message,)
        return cls(*values)
