from pathlib import Path
from jinja2 import Template

_ALL_TEMPLATES = {
        pth.stem: pth.absolute() for pth in Path(__file__).parent.glob('*.j2')}

def get_template(key: str):
    jnj2 = _ALL_TEMPLATES.get(key)
    if jnj2 is None:
        return None
    return Template(jnj2.open('r').read())
