""" Atomic functions that might be used everywhere else
"""
import re
from typing import Tuple


def extract_message_link(string: str) -> Tuple[Tuple[int], str]:
    """
    Extracts ID to Discord message from string

    Parameters
    ----------
    string : str
        String to parse. It must contain fully qualified URL
        to message

    Returns
    -------
    ids: Tuple[int]
        Tuple (gid, cid, mid) of three int with the Guild id gid,
        the Channel id cid and Message id mid. Emtpy tuple, if
        there is no URL to a message in the string
    stripped: str
        Same as unput string, but stripped by the URL (if any)
        Equivalent to: string.replace(<URL>, '')

    Note
    ----
    More than one links in string results in not defined behavior
    """
    pat = (r'(?P<pre>.*?)http.*?discord/(?P<gid>\d+)/(?P<cid>\d+)/'+
           r'(?P<mid>\d+)(?P<post>.*)$')
    ret = re.compile(pat, re.IGNORECASE).match(string)

    if ret is None:
        ids = ()
        stripped = string
    else:
        pre, gid, cid, mid, post = ret.groups()
        ids = (gid, cid, mid)
        stripped = pre + post

    return ids, stripped
