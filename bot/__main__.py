"""
Bot assembly

https://discord.com/api/oauth2/authorize?
client_id=821501112423022652&
permissions=269716544&
scope=bot

Premissions:
    Manage Roles
    View Channels
    Send Messages
    Attach Files
    Read Message History
    Mention Everyone
    Add Reactions
    Connect
"""

import os
import sys
import discord
import logging
from discord.ext import commands

from .commandgroups import (
    AnnounceAdmin, AnnounceUser, PlayTag, Debug, Checker,
    HouseKeeper, HouseKeeper2, Jokes, Util)

from .commandgroups.announce2 import (
    AnnounceManager as Ann2Man, AnnounceUser as Ann2Usr,
    AnnounceCommands as Ann2Cmd)


def get_bot(debug_id=None, jokes=False, announce_cid=None, log_cid=None):
    """ get a standard bot
    """
    intents: discord.Intents = discord.Intents.default()
    intents.members = True
    intents.typing = False
    intents.presences = False

    bot = commands.Bot(command_prefix='$',
                       description='Beim Klabautermann...', intents=intents)

    if debug_id is not None:
        Checker.allow_debug_id(int(debug_id))
        bot.add_cog(Debug(bot=bot))
        bot.add_cog(Util()) # only version atm

    if announce_cid is not None:
        kwargs = dict(announce_cid=int(announce_cid), bot=bot)

        # old announcement system
        bot.add_cog(AnnounceAdmin(**kwargs))
        bot.add_cog(AnnounceUser(**kwargs))

        # new announcement system
        bot.add_cog(Ann2Man(**kwargs))
        bot.add_cog(Ann2Cmd(**kwargs))
        bot.add_cog(Ann2Usr(**kwargs))

    if log_cid is not None:
        log_cid = int(log_cid)
        bot.add_cog(HouseKeeper(bot, log_cid=log_cid))

    if jokes:
        bot.add_cog(PlayTag(bot, tagged_role='Du bist!'))
        bot.add_cog(Jokes())

    bot.add_cog(HouseKeeper2())

    return bot


def run_bot():
    """Run bot from environment
    """

    bot = get_bot(
        debug_id=os.environ.get('SHIVER_DEBUGID', None),
        announce_cid=os.environ.get('SHIVER_ANNOUNCE', None),
        jokes=True,
        log_cid=os.environ.get('SHIVER_LOG', None),
    )

    if '--debug' in sys.argv:
        LOG = logging.getLogger('Shiver')
        LOG.addHandler(logging.StreamHandler())
        LOG.setLevel(logging.DEBUG)
        LOG.debug('Logging on')
    
    token = os.environ.get('SHIVER_TOKEN', None)
    if token is None:
        raise ValueError('Can not run bot with empty token!')

    bot.run(str(token))


if __name__ == '__main__':
    run_bot()
