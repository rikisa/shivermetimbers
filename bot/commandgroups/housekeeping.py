import re
from datetime import datetime
import pytz
from functools import wraps

import discord
from discord.ext import commands, tasks
from discord.utils import get


class HouseKeeper(commands.Cog):

    control_role = 'Strippenzieher'
    _chores = []

    @commands.Cog.listener()
    async def on_ready(self):
        """
        Update all announcements from this bot in the announcment channel
        """
        await self.run_errands.start()

    @tasks.loop(hours=8.0)
    async def run_errands(self):
        for chore in self._chores:
            await chore()

    @commands.command()
    @commands.has_role(control_role)
    async def force(self, ctx: commands.Context):
        """run all chores
        """
        for chore in self._chores:
            await ctx.send(f"Starting '{chore}'")
            await chore()

    @commands.command()
    @commands.has_role(control_role)
    async def toggle(self, ctx: commands.Context):
        if self.run_errands.is_running():
            await ctx.send('Ich höre ja schon auf')
            self.run_errands.cancel()
            self._log_chan = None
        else:
            await ctx.send('Ok, ich mache dann mal los')
            self.run_errands.start()
            self._log_chan = ctx.channel
    
    @classmethod
    def register(cls, func, args=(), kwargs=None):
        if kwargs is None:
            kwargs = {}
        @wraps(func)
        async def _wrapped():
            ret = await func(*args, **kwargs)
            return ret
        cls._chores.append(_wrapped)
