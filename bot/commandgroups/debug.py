"""Debugging Commands
"""
import logging
from pathlib import Path
from discord.ext import commands
import time
import asyncio

# error logging
import traceback
from io import StringIO

from .housekeeping import HouseKeeper


LOG = logging.getLogger('Shiver')


class Checker:
    """Manage global state for checks on class level
    """

    _debug_id = None

    @classmethod
    def allow_debug_id(cls, user_id):
        cls._debug_id = int(user_id)

    @classmethod
    def is_debugger(cls, ctx: commands.Context):
        """For checks of commands
        """
        return ctx.author.id == cls._debug_id


class Debug(commands.Cog):
    """Debugging commands
    """

    def __init__(self, bot: commands.Bot):
        self._last_err_tb = 'None'
        self._bot = bot
        self.member = 0
        HouseKeeper.register(self._logsmth, (), dict(msg='Autotest'))

    @commands.command()
    @commands.check(Checker.is_debugger)
    async def reconnect(self, ctx: commands.Context):
        """Get last error trace and post it
        """
        await self._bot.close()
        await self._bot.connect()

    @commands.command()
    @commands.check(Checker.is_debugger)
    async def err(self, ctx: commands.Context):
        """Get last error trace and post it
        """
        dm_ch = ctx.author.dm_channel
        if dm_ch is None:
            dm_ch = await ctx.author.create_dm()
        await dm_ch.send(self._last_err_tb)

    @commands.command()
    @commands.check(Checker.is_debugger)
    async def raiserr(self, ctx: commands.Context, *args, **kwargs):
        """Provoke an error
        """
        txt = ', '.join([str(ctx), str(args), str(kwargs)])
        raise ValueError(txt)

    @commands.command()
    @commands.check(Checker.is_debugger)
    async def dumpthis(self, ctx: commands.Context, link):
        """Dump stuff plz
        """
        with Path('debug.txt').open('wb') as dmp:
            dmp.write(b'Getting COG\n')
            acog = self._bot.get_cog('AnnounceAdmin')
            trgt_msg = await acog._fetch_msg(link)
            dmp.write(bytes(trgt_msg.content, 'utf8'))
    
    def dump_error(self, error: Exception):
        """ Write error trace into buffer
        """
        fd = StringIO()
        traceback.print_exception(type(error), error,
            error.__traceback__, file=fd)
        fd.seek(0)
        self._last_err_tb = fd.read()
        fd.close()
        LOG.debug(self._last_err_tb)

    @commands.command()
    @commands.check(Checker.is_debugger)
    async def nuke(self, ctx: commands.Context, limit: int):
        """Just nuke everything
        """
        cnt = 0
        tasks = []

        LOG.debug('Start nuke')
        t0 = time.time()
        async for msg in ctx.channel.history(limit=limit):
            try:
                cnt += 1
                if cnt % 10 == 0:
                    LOG.debug('Nuked %d/%d', cnt, limit)
                await msg.delete()
            except:
                LOG.debug(f'Could not remove {str(msg)}')

        dt = time.time() - t0
        LOG.debug('Done nuking! (%d/%d ~%.1f/s in %.1f s)', cnt, limit,
                    cnt / dt, dt)
        ack = await ctx.send(
            f'Nuke {cnt}/{limit} messages in {ctx.channel}, nuking myself now')

        # loop = asyncio.get_running_loop()
        await asyncio.sleep(5)
        await ack.delete()

    @commands.command()
    async def logsmth(self, ctx: commands.Context, msg: str):
        await self._logsmth(msg)

    @commands.command()
    async def spam(self, ctx: commands.Context, msg: str):
        """Just spam
        """
        await ctx.send(f'{self.member}: {msg}')
        self.member += 1
    
    async def _logsmth(self, msg: str):
        """Just spam to debug log
        """
        LOG.debug(msg)
