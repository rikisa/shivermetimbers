"""Fun stuff
"""
import re
from urllib import request
import random as rnd

import discord
from discord.ext import commands
from discord.utils import get
from bs4 import BeautifulSoup as BS

from .._version import get_versions as get_version


class PlayTag(commands.Cog):
    """
    Commands to umpire a game of tag in the Guild
    """

    def __init__(self, bot: commands.Bot, tagged_role: str):
        self._bot = bot
        self._tagged_role = tagged_role
        self._idre = re.compile(r'\<\@\!?(?P<uid>\d*)\>')

    @commands.command(
        brief='Wer ist dran?',
        description='Stupps denjenigen an, der ist...',
        help='')
    async def werist(self, ctx: commands.Context):
        tag_role: discord.Role = get(ctx.guild.roles, name=self._tagged_role)
        tagged_user: discord.User = tag_role.members[0]
        await ctx.send(f'Hey, du bist {tagged_user.mention}, mach mal was!')

    @commands.command(
        brief='Fang jemanden!',
        description='Wenn du bist, fang jemanden mit diesem Kommando!',
        help='<name> muss eine @mention sein!')
    async def dubist(self, ctx: commands.Context, name: str):
        """
        Command to mark another player: $dubist @<Name>

        Evaluates the issuing context. If the tagged player calls
        the command, the tag is passed on to name

        Parameters
        ----------
        ctx : commands.Context
            Discord command context, used to evaluate if the command
            was issued by a valid player
        name : str
            One parameter being the name. Name is a mention, only the
            name is replied. If it is just a raw name, it is used to
            query the user, and the bot relplies with a mention

        """

        tag_role = get(ctx.guild.roles, name=self._tagged_role)

        if tag_role not in ctx.author.roles:
            await ctx.send(f'Hey {ctx.author.name} du bist doch gar nicht dran!')
            return

        if 'everyone' in name:
            await ctx.send('Nice try, no banana...')
            return

        try:
            user_id_new = int(self._idre.match(name).group(1))
            now_its = await ctx.guild.fetch_member(user_id_new)
            if now_its is None:
                raise ValueError
        except (ValueError, TypeError, AttributeError):
            await ctx.send(f'Wer oder was ist ist {name}?')
            return

        if now_its.bot:
            await ctx.send('Alle bots sind im blubb!')
            return

        if tag_role is None:
            tag_role = await ctx.guild.create_role(name=self._tagged_role,
                permissions=discord.Permissions.none(),
                colour=discord.Colour.from_rgb(18, 89, 126),
                hoist=True, mentionable=True)
            await now_its.add_roles(tag_role, reason='Du bist jetzt')
            return

        await ctx.author.remove_roles(tag_role, reason='Du bist es nicht mehr!')
        await now_its.add_roles(tag_role, reason='Du bist jetzt')

        if ctx.author == now_its:
            await ctx.send(
                f'{ctx.author.name} spielt fangen mit sich selbst...')
        else:
            await ctx.send(
                f'{ctx.author.name} hat {now_its.name} gefangen!')

class Jokes(commands.Cog):

    @commands.command(
        brief='Erzähl einen Witz',
        description='Zufälliger witz von spick.ch (Achtung: Flachwitzte)',
        help='HAHA!')
    async def witz(self, ctx: commands.Context):
        page = rnd.randint(1, 100)
        url = f'https://www.spick.ch/dein-spick/witze/page/{page}/' 

        with request.urlopen(url) as response:
            bs = BS(response.read(), features='html.parser')

        cards = bs.find_all(attrs={'class': 'overview-item-text'})
        rnd.shuffle(cards)
        joke = cards[0].find(name='span').text
        await ctx.send(joke)


class Util(commands.Cog):

    @commands.command()
    async def version(self, ctx: commands.Context):
        """Return version string
        """
        await ctx.send(f"ShiverMeTimbers {get_version()['version']}")
