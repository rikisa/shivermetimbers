""" Commands and listeners for composition of the bot
"""
from datetime import datetime, timedelta as td
import traceback
from io import StringIO
from urllib.parse import urlparse

import pytz
import discord
from discord.ext import commands

from ..parsing import Announcement, SessionTime
from ..util import extract_message_link


class AnnounceAdmin(commands.Cog):
    """
    Create and parse message based announcments
    """
    control_role = 'Strippenzieher'

    def __init__(self, bot: commands.Bot, announce_cid: int):
        self._bot = bot
        self._announc_cid = announce_cid

    async def _fetch_ref(self, argstring) -> discord.Message:
        #TODO fetch ref funtion
        ref, argstring = extract_message_link(argstring)
        if ref == ():
            raise commands.MessageNotFound(argstring)
        gid, cid, mid = ref
        try:
            trgt_g = self._bot.get_guild(int(gid))
            trgt_ch = trgt_g.get_channel(int(cid))
            return await trgt_ch.fetch_message(int(mid))
        except:
            raise commands.MessageNotFound('/'.join([str(r) for r in ref]))

    async def _fetch_msg(self, url: str):
        ret = urlparse(url.strip(' /\n'))
        if 'discord.com' not in ret.netloc:
            return None
        path = ret.path.strip('/')
        try:
            chan, gid, cid, mid = path.split('/')
            if chan != 'channels':
                raise ValueError
            gid, cid, mid = [int(v) for v in (gid, cid, mid)]
        except ValueError:
            raise commands.MessageNotFound(path)

        trgt_g = self._bot.get_guild(gid)
        trgt_ch = trgt_g.get_channel(cid)
        return await trgt_ch.fetch_message(mid)

    @commands.Cog.listener()
    async def on_ready(self):
        """
        Update all announcements from this bot in the announcment channel
        """
        chan = self._bot.get_channel(self._announc_cid)
        if chan is None:
            return
        async for msg in chan.history(limit=50):
            # check bot
            if msg.author != self._bot.user:
                continue
            # announcement
            try:
                await self._update_announcement(msg)
            except:
                print('Error in on ready!')

    async def _update_announcement(self, msg):
        ann = Announcement.from_msg(msg)
        if ann is None:
            return
        #TODO for now, this is enough, must be separated if a command
        #exists at some point again
        ann.mentions = set([])
        await ann.parse_reactions(ignored=[self._bot.user.mention])
        await msg.edit(content=ann.to_markdown())

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, event: discord.RawReactionActionEvent):
        """
        Update message based on added reactions
        """
        chan = self._bot.get_channel(event.channel_id)
        msg = await chan.fetch_message(event.message_id)
        await self._update_announcement(msg)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, event: discord.RawReactionActionEvent):
        """
        Update message based on added reactions
        """
        chan = self._bot.get_channel(event.channel_id)
        member, = await chan.guild.query_members(user_ids=[event.user_id])
        msg = await chan.fetch_message(event.message_id)
        await self._update_announcement(msg)

    # @commands.Cog.listener()
    # async def on_command_error(self, ctx, error):

    #     # This prevents any commands with local handlers being handled here in on_command_error.
    #     if hasattr(ctx.command, 'on_error'):
    #         return

    #     if isinstance(error, commands.MissingRole) or isinstance(error, commands.MissingPermissions):
    #         await ctx.send('Hey, das darfst du nicht!')
    #         return

    #     if isinstance(error, commands.CommandNotFound):
    #         await ctx.send("Den Befehl gibt es nich'...")
    #         return

    #     if isinstance(error, commands.CheckFailure):
    #         await ctx.send('Was immer du versuchst, das darfst DU nicht...')
    #         return

    #     if isinstance(error, commands.MessageNotFound):
    #         await ctx.send(
    #             f'Konnte die Nachricht _{error.argument}_ nicht finden (gelöscht?)')
    #         return

    #     await ctx.send(
    #         'Es liegt an mir, nicht an dir... Liebst du mich trotzdem noch?\n' +
    #         '_500: internal error_')

    #     debug_cog = self._bot.get_cog('Debug')
    #     if debug_cog is not None:
    #         fd = StringIO()
    #         traceback.print_exception(type(error), error,
    #             error.__traceback__, file=fd)
    #         fd.seek(0)
    #         debug_cog._last_err_tb = fd.read()
    #         fd.close()

    @commands.command(
        brief='Neuer Termin',
        description=('Erstellt einen neune Termin an'+
                     ' und postet ihn direkt'),
        help=('Zeitpunkt ist das Datum und Uhrzeit im Format: TT.MM. HH:MM' +
              'Link ist optionaler link zu einer Nachricht, die als Vorlage' +
              'Verwendet wird. Bot muss die Nachricht lesen können.'),)
    @commands.has_role(control_role)
    async def gesetzt(self, ctx: commands.Context, titel, datum, uhrzeit,
                      link, *args):
        args = (titel, datum, uhrzeit, link) + args
        argstring = ' '.join(args)
        link = argstring.split(' ')[-1]

        # trgt_msg = await self._fetch_ref(argstring)
        trgt_msg = await self._fetch_msg(link)
        if trgt_msg is None:
            body = f'Ich bin dabei --> {Announcement._count_symbol}\n'
            body += f'Ich bin raus --> {Announcement._out_symbol}\n'
        else:
            body = trgt_msg.content

        date = SessionTime.from_str(argstring)
        if date is None:
            await ctx.send(f'Datum "{argstring}" ist ungültig')
            return

        name = date.pre
        if name is None or name == '':
            await ctx.send('Ungüliger oder kein Name!')
            return
        name = name.strip('')

        ann = Announcement(
            name=name,
            date=date,
            body=body,
            mentions=[]
        )

        chan = self._bot.get_channel(self._announc_cid)
        msg = await chan.send(ann.to_markdown())
        await msg.add_reaction(Announcement._count_symbol) # '\u2705'
        await msg.add_reaction(Announcement._out_symbol) # '\u274c'

    @commands.command(
        brief='Verschieb Termin',
        description=('Verschiebt einen bereits erstellten Termin'),
        help=('Zeitpunkt ist das Datum und Uhrzeit im Format: TT.MM. HH:MM' +
              'Link zu Terminankündigung. bot kann nur seine eigenen' +
              ' ankündigungen verschieben!'),)
    @commands.has_role(control_role)
    async def verschieben(self, ctx: commands.Context, titel, datum, uhrzeit,
                      link, *args):
        args = (titel, datum, uhrzeit, link) + args
        argstring = ' '.join(args)
        link = argstring.split(' ')[-1]
        """
        Move and rename announcment, with new_name, date, time, link as arguments
        """
        argstring = ' '.join(args)

        trgt_msg = await self._fetch_msg(link)

        if trgt_msg is None:
            await ctx.send(f'Ungültige Nachricht (eventuel in privatem Kanal?)')
            return None

        if trgt_msg.author.id != self._bot.user.id:
            await ctx.send('Ich kann nur von mir erstellte Nachrichten' +
                           f'ändern. Die angegebene gehört {trgt_msg.author.name}')
            return

        ann = Announcement.from_msg(trgt_msg)
        if ann is None:
            await ctx.send('Konnte nicht verschieben...')
            return

        new_date = SessionTime.from_str(argstring)
        if new_date is None:
            await ctx.send(f'Datum "{argstring}" ist ungültig')
            return

        if new_date != ann.date:
            ann.date = new_date

        new_name = new_date.pre
        if new_name is not None and new_name != '':
            ann.name = new_name

        #TODO for now, this is enough, must be separated if a command
        # exists at some point again
        await trgt_msg.edit(content=ann.to_markdown())
        await ctx.send(f'Geändert zu Name: _{ann.name}_ Datum/Uhrzeit:'+
                       f' _{ann.date}_')

    @commands.command(
        brief='Ändert Termin Text',
        description=('Ändert Text in einem bereits erstellten Termin'),)
    @commands.has_role(control_role)
    async def editieren(self, ctx, link_announce: str, link_update: str):
        """
        Edit announced message, expects two links. One to original, other to update
        """

        trgt_msg = await self._fetch_msg(link_announce)
        update_msg = await self._fetch_msg(link_update)

        err = ''
        if trgt_msg is None:
            err += f'Ungültige Nachricht zu editieren (eventuel in privatem Kanal?)'
        if update_msg is None:
            err += f'Ungültige Nachricht für neuen Text'

        if err != '':
            await ctx.send(err)
            return

        if trgt_msg.author.id != self._bot.user.id:
            await ctx.send('Ich kann nur von mir erstellte Nachrichten' +
                           f'ändern. Die angegebene gehört {trgt_msg.author.name}')
            return

        ann = Announcement.from_msg(trgt_msg)
        if ann is None:
            await ctx.send('Scheinbar keine gültige Terminankündigung...')
            return


        ann.body = update_msg.content

        #TODO for now, this is enough, must be separated if a command
        # exists at some point again
        await trgt_msg.edit(content=ann.to_markdown())
        await ctx.send(f'Nachrichttext geändert zu:\n>>> {ann.body}')

    @commands.command(
        brief='Kleiner Contdown zum nächsten Termin',
        description=('Erinnert alle an den nächsten Termin innerhalb der' +
                     ' nächsten Tage/Stunden'),)
    @commands.has_role(control_role)
    async def ansagen(self, ctx: commands.Context, tage: str = None):
        try:
            days = float(tage.replace(',', '.'))
        except (AttributeError, ValueError):
            await ctx.send(
                'Diggi, must Tage angeben. Halbe Tage sind auch ok. ' +
                'z.B. `$ansagen 0.3` sagt den nächste Termin innerhalb ' +
                'der nächsten 8 Stunde')
            return

        planned = []
        chan = self._bot.get_channel(self._announc_cid)

        berlin_time = pytz.timezone('Europe/Berlin')
        now = pytz.utc.localize(datetime.utcnow())
        then = now + td(seconds=(days * 24 * 60 * 60))

        # TODO make async iterator
        async for msg in chan.history(limit=50):
            # check bot
            if msg.author != self._bot.user:
                continue
            # parse an, get now and then based
            ann = Announcement.from_msg(msg)
            if ann is None:
                continue

            planned_date = berlin_time.normalize(
                ann.date.datetime).astimezone(pytz.utc)

            if now < planned_date < then:
                planned.append(ann)

        if not planned:
            await ctx.send('Keine Termine geplant, innerhalb der nächtesn' +
                           f' {days} Tage')
            return

        planned.sort(key=lambda ann: ann.date)

        # for ann in planned:
        ann = planned[0]
        # await ctx.send([(ann.name, ann.date) for ann in planned])
        dnext = ann.date.datetime - now
        seconds = int(dnext.total_seconds())

        days, seconds = divmod(seconds, 24 * 60 * 60)
        hours, seconds = divmod(seconds, 60 * 60)
        minutes, seconds = divmod(seconds, 60)

        if len(ann.mentions) >= 1:
            msg = f'Holla, am {ann.date} startet:\n\n'
            msg += f'_ _      **_{ann.name}_**\n\n'
            msg += f'mit: {",".join(ann.mentions)}\n\n'
            msg += f'in ` '
            if days == 1:
                msg += f'{days} Tag '
            if days > 1:
                msg += f'{days} Tagen '
            if hours == 1:
                msg += f'{hours} Stunde '
            if hours > 1:
                msg += f'{hours} Stunden '
            if minutes == 1:
                msg += f'{minutes} Minute '
            if minutes > 1:
                msg += f'{minutes} Minuten '
            if not days and not hours and not minutes:
                msg += f'JETZT '
            msg += '`'
        else:
            msg = f'Es ist _{ann.name}_ am _{ann.date}_ gesetz, '
            msg += f'aber niemand hat sich bisher gemeldet!'
            await ctx.send(msg)
            return

        await ctx.send(msg)

    @commands.command(brief='Lösch Termine', description=('Löscht alle Termine'+
        ' die älter als angegeben Tage/Stunden sind'))
    @commands.has_role(control_role)
    async def staubwedel(self, ctx: commands.Context, tage: str = None):
        await self._staubwedel(ctx, tage, verbose=2)

    async def _staubwedel(self, ctx: commands.Context, tage: str = None,
                          verbose: int = 0):
        try:
            days = float(tage.replace(',', '.'))
        except (AttributeError, ValueError):
            await ctx.send(
                'Diggi, must Tage angeben. Halbe Tage sind auch ok. ' +
                'z.B. `$staubwedel 0.3` sagt den nächste Termin innerhalb ' +
                'der nächsten 8 Stunde')
            return
        if days < 0:
            await ctx.send('Nice try, no banana...')
            return

        delete = []
        chan = self._bot.get_channel(self._announc_cid)

        berlin_time = pytz.timezone('Europe/Berlin')
        now = pytz.utc.localize(datetime.utcnow())
        bestbefore = now - td(seconds=(days * 24 * 60 * 60))

        # TODO make async iterator
        async for msg in chan.history(limit=50):
            # check bot
            if msg.author != self._bot.user:
                continue
            # parse an, get now and then based
            ann = Announcement.from_msg(msg)
            if ann is None:
                continue

            planned_date = berlin_time.normalize(
                ann.date.datetime).astimezone(pytz.utc)

            if planned_date < bestbefore:
                delete.append((ann.name, ann.date))
                await msg.delete()

        delete.sort(key=lambda dat: dat[-1].datetime)
        delete = [f'{n} {d}' for n, d in delete]

        if delete and verbose > 0:
            rowed = '- ' + '\n- '.join(delete)
            ret = f'Ich habe folgende Termine gelöscht:\n>>> {rowed}'
            await ctx.send(ret)
        elif verbose > 1:
            ret = 'Nichts gelöscht...'
            await ctx.send(ret)


class AnnounceUser(commands.Cog):
    """
    Create and parse message based announcments
    """

    def __init__(self, bot: commands.Bot, announce_cid: int):
        self._bot = bot
        self._announc_cid = announce_cid

    @commands.command(
        brief='Liste aller Termine',
        description=('Schickt dir eine Liste aller Termine, für die du dich'+
                     ' angemeldet hast als DM'),
        help='')
    async def wasgeht(self, ctx: commands.Context):
        """
        Parses all anouncments, and returns any the command issuer reacted to
        """

        dm_ch = ctx.author.dm_channel
        if dm_ch is None:
            dm_ch = await ctx.author.create_dm()

        dabei = []
        chan = self._bot.get_channel(self._announc_cid)
        #TODO make async iterator
        async for msg in chan.history(limit=50):
            # check bot
            if msg.author != self._bot.user:
                continue
            ann = Announcement.from_msg(msg)
            if ann is None:
                continue
            if ctx.author.mention in ann.mentions:
                dabei.append([ann.name, ann.date])
        dabei.sort(key=lambda it: it[-1])
        await ctx.send('Hast DM-Post...!')
        if dabei:
            ret = 'Du hast dich für folgende Termine gemeldet:\n>>> '
            for name, ann.date in dabei:
                ret += f'**{name}** am {str(ann.date)}\n'
        else:
            ret = 'Du hast momentan nirgends zugesagt!'
        await dm_ch.send(ret)
