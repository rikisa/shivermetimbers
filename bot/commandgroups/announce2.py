""" Commands and listeners for Announcment parsing
"""
from typing import Tuple, List
from datetime import datetime, timedelta as td
from urllib.parse import urlparse
from pprint import pprint
from io import StringIO
import math
import re
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
import base64
from lark.exceptions import UnexpectedInput, VisitError

from jinja2 import Template, TemplateError

import pytz
import discord
from discord.ext import commands
import logging
import os
# from uuid import uuid4, UUID

from ..derp import PostTransformer
from ..templates import get_template
from  .housekeeping import HouseKeeper


LOG = logging.getLogger('Shiver')


class Emojis:
    ok = '\u2705'


class LinkSignature:
    
    _pre = Emojis.ok
    _id_conn = '/'
    _id_pat = re.compile(rf'(\d+){_id_conn}(\d+){_id_conn}(\d+)')
    # _part_conn = ':'
    
    # byte packing order
    _byo = 'little'
    # encryption
    _iv_len = 8
    _secret = bytes.fromhex(
        os.environ.get('SHIVER_LINK_NONCE', os.urandom(4).hex()))

    def __init__(self, gid, cid, mid, encrypt_sig=False, iv=None):
        self.gid = gid
        self.cid = cid
        self.mid = mid
        self.encrypt = encrypt_sig
        
        self._use_bytes = math.ceil(
            max(math.log2(_id) for _id in self.id_values) / 8)

        self._signature = None
        self._iv = iv

    @property
    def id_values(self):
        return (self.gid, self.cid, self.mid)

    @property
    def id_string(self):
        return self._id_conn.join(str(_id) for _id in self.id_values)

    @property
    def id_bytes(self):
        id_bytes = [i.to_bytes(self._use_bytes, self._byo) \
                        for i in self.id_values]
        return b''.join(id_bytes)

    @property
    def signature(self):
        if self._signature is None:

            if self.encrypt:
                if self._iv is None:
                    self._iv = os.urandom(self._iv_len)
                raw_signature = self.encrypt_bytes(
                    self._use_bytes.to_bytes(1, self._byo)
                    + self.id_bytes, iv=self._iv)
            else:
                raw_signature = (
                    self._use_bytes.to_bytes(1, self._byo)
                    + self.id_bytes)

            raw_signature = self.encrypt.to_bytes(1, self._byo) + raw_signature

            self._signature = base64.b85encode(raw_signature)

        return self._signature.decode('ascii')

    @classmethod
    def get_cipher(cls, iv: bytes) -> Cipher:
        return Cipher(algorithms.Blowfish(cls._secret), modes.CFB(iv))
    
    @classmethod
    def decrypt_bytes(cls, sig_bytes, iv):
        decryptor = cls.get_cipher(iv).decryptor()
        return decryptor.update(sig_bytes) + decryptor.finalize()

    @classmethod
    def encrypt_bytes(cls, sig_bytes, iv):
        encryptor = cls.get_cipher(iv).encryptor()
        encrypted_bytes = encryptor.update(sig_bytes) + \
            encryptor.finalize()
        return iv + encrypted_bytes
    
    @classmethod
    def from_signature(cls, signature):

        raw_signature = base64.b85decode(signature)

        encrypt = bool(raw_signature[0])
        
        if encrypt:
            _iv = raw_signature[1:cls._iv_len+1]
            raw_signature = cls.decrypt_bytes(
                raw_signature[cls._iv_len+1:], iv=_iv)
        else:
            raw_signature = raw_signature[1:]
            _iv = None

        use_bytes = raw_signature[0]
        id_bytes = raw_signature[1:]
        id_values = tuple(int.from_bytes(id_bytes[i:i+use_bytes], cls._byo) \
                            for i in range(0, 3*use_bytes, use_bytes))

        try:
            # _ids = [int(_id) for _id in cls._id_pat.match(id_string).groups()]
            inst = cls(*id_values, encrypt_sig=encrypt)
            inst._iv = _iv
            return inst
        except (AttributeError, # not matching
                UnicodeDecodeError): # not valid signature
            return None

    @classmethod
    def from_string(cls, id_string: str, encrypt_sig=False):
        try:
            ids = cls._id_pat.match(id_string).groups()
            ids = [int(i) for i in ids]
            return cls(*ids, encrypt_sig=encrypt_sig)
        except (AttributeError, # not matching
                UnicodeDecodeError, # not valid signature
                TypeError): # invalid number of args
            return None

    @classmethod
    def from_message(cls, msg: discord.Message):
        return cls(gid=msg.guild.id, cid=msg.channel.id, mid=msg.id)
    
# TODO CACHING
# also a gazillion checks all over the place...
class LinkedMessages:

    def __init__(self, bot: discord.ext.commands.Bot):
        self._ann_msg: discord.Message = None
        self._link_msg: discord.Message = None
        self._tmpl_msg: discord.Message = None

        self._ann_pointer: LinkSignature = None
        self._tmpl_pointer: LinkSignature = None

        self._bot = bot

    @property
    def _ann_ch(self):
        if self._ann_msg is None:
            return None
        return self._ann_msg.channel

    @property
    def _tmpl_ch(self):
        if self._tmpl_msg is None:
            return None
        return self._tmpl_msg.channel
    
    def __str__(self):
        if self._tmpl_msg is not None:
            basics, _, _ = self.get_parsed()
        else:
            basics = {}
        
        return get_template('linked_msg').render(basics=basics)

    def finalize_ann(self, ann_content: str):
        return ann_content + f'\n\n\n||{self._tmpl_pointer.signature}||'

    async def publish(self, tmpl_msg: discord.Message,
                      channel: discord.TextChannel):
        """Initial post and link post
        
        bootstrapping everything, expect no messages at all
        """
        self._tmpl_msg = tmpl_msg

        ann_content, _, emojis = await self.render()

        self._ann_msg: discord.Message = await channel.send(
            content=ann_content)

        ann_pointer = LinkSignature(gid=self._ann_msg.guild.id,
                                    cid=self._ann_msg.channel.id,
                                    mid=self._ann_msg.id,
                                    encrypt_sig=False)

        tmpl_pointer = LinkSignature(gid=self._tmpl_msg.guild.id,
                                     cid=self._tmpl_msg.channel.id,
                                     mid=self._tmpl_msg.id,
                                     encrypt_sig=False)
        self._ann_pointer = ann_pointer
        self._tmpl_pointer = tmpl_pointer

        # reply
        self._link_msg = await self._tmpl_msg.reply(ann_pointer.id_string)
        # add signature
        signed_ann_content = self.finalize_ann(ann_content)
        await self._ann_msg.edit(content=signed_ann_content)

        for emoji in emojis:
            await self._ann_msg.add_reaction(emoji)

    async def reaction_changed(self, ann_msg: discord.Message):
        """ equal to semthing like: set from annnounced
        """
        self._ann_msg = ann_msg

        LOG.debug('Reaction changed!')

        await self.set_for_announced(ann_msg)

        await self._set_links()
        await self.update()

    def get_parsed(self):
        #TODO cache me
        parsed_template = PostTransformer.parse_post(self._tmpl_msg.content)

        basics = parsed_template['basics']
        template = parsed_template['template']
        emojis = parsed_template['emojis']

        return basics, template, emojis
        
    async def render(self) -> str:
        """parsing happens here"""

        basics, template, emojis = self.get_parsed()
        reactions = await self.get_reactions()

        for emo in emojis:
            reactions.setdefault(emo, [])
            # reactions.setdefault(f'@{emo}', [])
            # reactions.setdefault(f'#{emo}', 0)
        
        # erros must be catched ouside
        tmpl = Template(template)
        return tmpl.render(basics=basics, reactions=reactions), basics, emojis

    async def depublish(self, tmpl_msg: discord.Message = None):
        """Initial post and link post
        """
        if tmpl_msg is not None:
            self._tmpl_msg = tmpl_msg
            await self._set_links()

        # seek stuff in templ
        if self._ann_msg is not None:
            if self._ann_msg.author == self._bot.user:
                try:
                    await self._ann_msg.delete()
                except discord.errors.NotFound:
                    pass
        if self._link_msg is not None:
            try:
                await self._link_msg.delete()
            except discord.errors.NotFound:
                pass

        self._ann_msg: discord.Message = None
        self._link_msg: discord.Message = None
        self._tmpl_msg: discord.Message = None

        self._link_sig: LinkSignature = None

    async def get_reactions(self):
        """Parse reactions from message
        """
        ret = {}

        LOG.debug('Parsing reactions...')
        if self._ann_msg is None:
            LOG.debug('...Announce is None!')
            return ret
        
        for reaction in self._ann_msg.reactions:
            users = await reaction.users().flatten()
            users = set(users)
            users.difference_update([self._bot.user])
            the_emoji = str(reaction.emoji)
            LOG.debug('Emoji %s', the_emoji)
            if users != set():
                ret[the_emoji] = list(usr.mention for usr in users)

        LOG.debug('Got reactoins %s', str(ret))
        return ret

    async def _from_pointer(self, ptr: LinkSignature):
        if ptr is None:
            return None
        guild = self._bot.get_guild(ptr.gid)
        channel = guild.get_channel(ptr.cid)
        ret = await channel.fetch_message(ptr.mid)
        return ret

    async def _set_links(self):
        ret = await self.set_for_tmpl(self._tmpl_msg)
        return ret

    async def set_for_tmpl(self, tmpl_msg: discord.TextChannel):
        """ Somethink like set from template, for that an link exists
        """

        async for link_cand in tmpl_msg.channel.history(limit=75):
            if link_cand.reference is None:
                continue
            # must point to the template message
            if link_cand.reference.message_id != tmpl_msg.id:
                continue
            # check if belongs to bot
            if link_cand.author != self._bot.user:
                continue
            await self.set_for_link(link_cand)
            return
 
        # nothing found...
        self._link_msg = None
    
    async def set_for_announced(self, ann_msg, missing_ok=False):

        self._ann_msg = ann_msg
        
        try:
            sig_string = self._ann_msg.content.split('||')[-2]
        except IndexError:
            raise ValueError('Not a valid announcement message')

        try:
            self._tmpl_pointer = LinkSignature.from_signature(sig_string)
            self._tmpl_msg = await self._from_pointer(self._tmpl_pointer)
        except discord.errors.NotFound as err:
            if not missing_ok:
                raise err

        LOG.debug('Resolve ann_msg %s pointing (%s) to tmpl %s',
                str(self._ann_msg.id), sig_string, str(self._tmpl_msg.id))

    async def set_for_link(self, link_msg: discord.Message):
        self._link_msg = link_msg
        self._tmpl_msg = link_msg.reference.resolved
        
        self._ann_pointer = LinkSignature.from_string(
            self._link_msg.content)
        self._tmpl_pointer = LinkSignature.from_message(self._tmpl_msg)
        self._ann_msg = await self._from_pointer(self._ann_pointer)

    async def update(self, tmpl_msg: discord.Message = None):

        if tmpl_msg is not None:
            self._tmpl_msg = tmpl_msg
            await self._set_links()

        if self._ann_msg is None:
            raise ValueError('Not published')

        ann_content, _, _ = await self.render()
        signed_ann_content = self.finalize_ann(ann_content)
        await self._ann_msg.edit(content=signed_ann_content)

    @staticmethod
    def is_link(link_cand: discord.Message):
        return LinkSignature._id_pat.match(link_cand.content) is not None

    def get_sessiontime(self):
        tz = pytz.timezone('Europe/Berlin')
        self._tz = tz
        now = tz.localize(datetime.now())
        basics, _, _ = self.get_parsed()
        try:
            date = basics['datum']
            time = basics['uhr']
            return now.replace(
                year=date.year, month=date.month, day=date.day,
                hour=time.hour, minute=time.minute)
        except KeyError:
            return None


class _AnnounceCog(commands.Cog):
    """All the announcments need this stuff
    """

    _control_role = 'Strippenzieher'

    def __init__(self, bot: commands.Bot, announce_cid: int):
        self._bot = bot
        # announc cid could be defined by depp message
        # self._announc_cid = announce_cid
        self.set_global_announce_cid(announce_cid)
        # self._control_role = 'Strippenzieher'
        self._control_channel = 'shivermetimbers'
        
        #TODO use me
        self._cached_msg = {}

    async def _get_announced(self) -> List:
        """link post wont be set!
        """
        # get all linked message
        linked_msg = []
        async for msg in self.announce_channel.history(limit=100):
            msg: discord.Message = msg
            # only consider bot posted replies
            if msg.author != self._bot.user:
                continue
            lmsg = LinkedMessages(self._bot)
            tmlp_chan = lmsg._tmpl_ch
            try:
                await lmsg.set_for_announced(msg)
            except (ValueError, discord.errors.NotFound):
                continue
            LOG.debug('Found announced message with template %s',
                        lmsg._tmpl_pointer)
            linked_msg.append(lmsg)
        
        #  # set the links
        #  if tmlp_chan is not None and find_links:
        #      async for link_msg_cand in tmpl_chan.history(limit=100):
        #          if link_msg_cand.author != self._bot.user or \
        #                  link_msg_cand.reference is None:
        #              continue
        #          for lmsg in linked_msg:
        #              if lmsg._tmpl_msg.id == link_msg_cand.message_id:
        #                  lmsg._set_links(link_msg_cand.resolved)
        #                  break

        return linked_msg

    @classmethod
    def set_global_announce_cid(cls, announce_cid: int):
        cls._announce_cid = announce_cid

    @property
    def control_role(self) -> str:
        return self._control_role

    @property
    def control_channel_name(self) -> str:
        return self._control_channel

    @property
    def announce_channel(self) -> discord.TextChannel:
        return self._bot.get_channel(self._announce_cid)

    def log_errortrace(self, err: Exception):
        debug_cog = self._bot.get_cog('Debug')
        if debug_cog is not None:
            debug_cog.dump_error(err)


class AnnounceManager(_AnnounceCog):
    """Listener that links new announcments and updates them

    no commands, just event handlers
    """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        HouseKeeper.register(self._remove_old)
        HouseKeeper.register(self._remind, args=(.3,))

    @commands.command(
        brief='Sende eine Nachricht mit anstehenden Terminen',
        help='')
    @commands.has_role(_AnnounceCog._control_role)
    async def erinnern(self, ctx: commands.Context, tage: str = '1'):
        """
        Parses all anouncments, and returns any the command issuer reacted to
        """
        try:
            days = float(tage.replace(',', '.'))
        except (AttributeError, ValueError):
            await ctx.send(
                'Tage muss im weiteren sinne eine Zahl sein... Halbe Tage ' +
                'sind auch ok. ' +
                'z.B. `$ansagen 0.3` sagt den nächste Termin innerhalb ' +
                'der nächsten 8 Stunde')
            return

        missing = await self._remind(days)
        if missing:
            suggest = ', '.join([f'#{m}' for m in missing])
            missing = ', '.join(missing)
            await ctx.send('In folgende Kanäle wurde nicht ' +
                           f'geposted: _{missing}_ \n' +
                           f'`#` vergessen? Editier Kanal mal zu _{suggest}_')

    async def _remind(self, days: float) -> List:
        tz = pytz.timezone('Europe/Berlin')
        self._tz = tz
        now = tz.localize(datetime.now())
        then = now + td(seconds=(days * 24 * 60 * 60))

        announce_messages = await self._get_announced()

        missing = []
    
        for lmsg in announce_messages:
            planned = lmsg.get_sessiontime()
            if planned is None:
                LOG.debug('Skip no timeslot %s', str(planned))
                continue
            if not (now < planned < then):
                LOG.debug('Skip not soon (%s < %s < %s)', str(now),
                        str(planned), str(then))
                continue

            basics, _, _ = lmsg.get_parsed()
            chan = basics.get('kanal')

            LOG.debug('Skip no channel %s', str(chan))
            
            if chan is None:
                missing.append(chan)
                continue

            LOG.debug('channel is %s', str(chan))

            try:
                chan_id = int(chan.strip('<#> '))
                send_to = await self._bot.fetch_channel(chan_id)
            except Exception as err:
                missing.append(chan)
                continue

            reactions = await lmsg.get_reactions()
            
            LOG.debug('Sending reminder for %s in %s', basics.get('titel', '?'),
                      str(chan))

            jnjtmpl = get_template('reminder')
            try:
                msg_content = jnjtmpl.render(basics=basics, reactions=reactions)
                await send_to.send(msg_content)
            except TemplateError as err:
                self.report_parsing_error('', lmsg._tmpl_ch, err)

        return missing

    @commands.command()
    async def remove_old(self, ctx: commands.Context):
        """
        Update message based on added reactions
        """
        await self._remove_old()

    async def _remove_old(self):
        tz = pytz.timezone('Europe/Berlin')
        self._tz = tz
        now = tz.localize(datetime.now())
        cutoff = now - td(days=1)

        announce_messages = await self._get_announced()
        
        LOG.debug('Old announcements message %d', len(announce_messages))
        deleted = []
        for lmsg in announce_messages:
            planned_for = lmsg.get_sessiontime()

            if planned_for is None:
                continue

            LOG.debug('planned for %s < %s = %s',
                      str(planned_for), str(cutoff), str(planned_for < cutoff))
            if planned_for < cutoff:
                deleted.append((str(lmsg), lmsg._tmpl_ch))
                await lmsg.depublish()
        
        for deleted_str, log_chan in deleted:
            ret = f'Gelöscht: `{deleted_str}`'
            await log_chan.send(ret)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, event: discord.RawReactionActionEvent):
        """
        Update message based on added reactions
        """
        LOG.debug('Reaction added')
        await self._reaction_changed(event)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, event: discord.RawReactionActionEvent):
        """
        Update message based on added reactions
        """
        LOG.debug('Reaction removed')
        await self._reaction_changed(event)

    async def _reaction_changed(self, event: discord.RawReactionActionEvent):

        ann_channel = self._bot.get_channel(event.channel_id)
        ann_msg = await ann_channel.fetch_message(event.message_id)

        if event.member == self._bot.user:
            LOG.debug('Ignore reaction of bot user')
            return

        linked = LinkedMessages(self._bot)
        await linked.reaction_changed(ann_msg)

    @commands.Cog.listener()
    async def on_message_edit(self, before: discord.Message,
                                    after: discord.Message):

        if after.author == self._bot.user:
            LOG.debug('Ignoring reaction to own edits/pins/publish')
            return

        if not self.control_role in [role.name for role in after.author.roles]:
            LOG.debug('Ignoring msg from non-authorized author: %s',
                      after.author.name)
            return

        if after.channel.name != self.control_channel_name:
            LOG.debug('Ignore message in wrong channel %s', after.channel.name)
            return

        LOG.debug('Message changed, processing...')

        # linked = LinkedMessages(
        #     announce_channel=self.announce_channel,
        #     template_channel=after.channel.name)
        linked = LinkedMessages(bot=self._bot)
        
        # new message
        if not before.pinned and after.pinned:
            LOG.debug('Publish message with content')
            # if rendered is None:
            #     return
            try:
                await linked.publish(after, self.announce_channel)
            except Exception as err:
                await self.report_parsing_error(
                    after.content, after.channel, err)
                self.log_errortrace(err)
                return
            return

        # smth with an already pinned one
        if before.pinned and not after.pinned:
            LOG.debug('Ignoring from now on content: %s', after.content)
            await linked.depublish(after)

        elif before.pinned and after.pinned:
            LOG.debug('Updating pinned message new content')
            try:
                await linked.update(after)
            except UnexpectedInput as err:
                await self.report_input_error(
                    after.content, after.channel, err)
                self.log_errortrace(err)
                return
            except VisitError as err:
                await self.report_parsing_error(
                    after.content, after.channel, err)
                self.log_errortrace(err)
                return
            except ValueError as err:
                self.log_errortrace(err)
                await after.channel.send(
                    'Bitte erst nochmal un-pinnen und wieder pinnen!')
        else:
            LOG.debug('Not reacting on edit!')

    # @staticmethod
    async def report_input_error(self,
        content: str, channel: discord.TextChannel, err: Exception):
        
        try:
            at_line = int(err.line)
            err_pointer = max(int(err.column) - 2, 0) * ' ' + '^^^'
            err_pointer += '*Error is here*'
        except Exception as tkn:
            err_pointer = '?'
            at_line = -1
            self.log_errortrace(tkn)

        higlighted = []
        for ln, cnt_line in enumerate(content.split('\n'), 1):
            higlighted.append(cnt_line)
            if ln == at_line:
                higlighted.append(err_pointer)

        cited = '```' + '\n'.join(higlighted) + '```'
        await channel.send(f'Input error!\n {cited}')

    async def report_parsing_error(self,
        content: str, channel: discord.TextChannel, err: Exception):

        if isinstance(err, TemplateError):
            ret = ('Jinja2 sagt, dein eingebettetes Template ist kaputt!\n'+
                   f'```{err.message}```')
            await channel.send(ret)
            return

        if hasattr(err, 'obj'):
            try:
                cited = '```'+': '.join([str(o) for o in err.obj.children])+'```'
            except Exception as tkn:
                self.log_errortrace(tkn)
                cited = '```' + str(err.obj) + '```'
            await channel.send(f'Parsing error! {cited}')
        else:
            try:
                await self.report_input_error(content, channel, err)
            except Exception as tkn:
                await channel.send(f'Parsing error! {str(err)}')

class AnnounceCommands(_AnnounceCog):
    """Commands for triggering posting and parsing of
    announcments
    """
    @commands.command(
        brief='Snipe alle announcments and links!',
        help='')
    @commands.has_role(_AnnounceCog._control_role)
    async def snipe(self, ctx: commands.Command):
        msgs = await self._get_announced()
        for msg in msgs:
            await msg.depublish(msg._tmpl_msg)

    @commands.command(
        brief='Force update/reparsing of all anouncments',
        help='')
    @commands.has_role(_AnnounceCog._control_role)
    async def forceupdate(self, ctx: commands.Command):
        msgs = await self._get_announced()
        for msg in msgs:
            await msg.update(msg._tmpl_msg)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):

        # This prevents any commands with local handlers being handled here in
        # on_command_error.
        if hasattr(ctx.command, 'on_error'):
            return

        if isinstance(error, commands.MissingRole) or isinstance(error,
                commands.MissingPermissions):
            await ctx.send('Hey, das darfst du nicht!')
            return

        if isinstance(error, commands.CommandNotFound):
            await ctx.send("Den Befehl gibt es nich'...")
            return

        if isinstance(error, commands.CheckFailure):
            await ctx.send('Was immer du versuchst, das darfst DU nicht...')
            return

        if isinstance(error, commands.MessageNotFound):
            await ctx.send(
                f'Konnte die Nachricht _{error.argument}_ nicht' +
                ' finden (gelöscht?)')
            return

        if isinstance(error, commands.UserInputError):
            await ctx.send(
                f'Du benutzt das falsch... Versuch mal `$help {ctx.command}`')
            return

        if isinstance(error, commands.CommandInvokeError):
            self.log_errortrace(error)
            if isinstance(error.original, TemplateError):
                ret = (f'Jinja2 dein eingebettetes template ist kaputt!\n'+
                       f'```{error.original.message}```')
            else:
                ret = (
                    f'Huch, hier ist was schief gegangne. Bitte sage schnell '+
                    f' einen Erwachsenen bescheid!\n```{type(error)} {error}```')

            await ctx.send(ret)
            return

        await ctx.send(
            'Es liegt an mir, nicht an dir... Liebst du mich trotzdem noch?\n' +
            f'```{type(error)} {error}```')

        self.log_errortrace(error)


class AnnounceUser(_AnnounceCog):
    """
    Create and parse message based announcments
    """
    pass

