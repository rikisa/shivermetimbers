import re
from datetime import datetime
import pytz

import discord
from discord.ext import commands, tasks
from discord.utils import get


class HouseKeeper(commands.Cog):

    control_role = 'Strippenzieher'

    def __init__(self, bot: commands.Bot, log_cid: int):
        self._bot = bot
        self._log_cid: int = log_cid

    @commands.Cog.listener()
    async def on_ready(self):
        """
        Update all announcements from this bot in the announcment channel
        """
        await self.run_errands.start()

    @tasks.loop(hours=8.0)
    async def run_errands(self):
        log_chan = self._bot.get_channel(self._log_cid)
        cog = self._bot.get_cog('AnnounceAdmin')
        # await log_chan.send('Autobot läuft...')
        await cog._staubwedel(ctx=log_chan, tage='1', verbose=1)

    @commands.command()
    @commands.has_role(control_role)
    async def autobot(self, ctx: commands.Context):
        if self.run_errands.is_running():
            await ctx.send('Ich höre ja schon auf')
            self.run_errands.cancel()
            self._log_chan = None
        else:
            await ctx.send('Ok, ich mache dann mal los')
            self.run_errands.start()
            self._log_chan = ctx.channel

    @commands.command()
    async def wannisthausputz(self, ctx: commands.Context):
        if self.run_errands.next_iteration is None:
            await ctx.send('Autobot ist nicht an')
            return

        nxt: datetime = self.run_errands.next_iteration
        nxt = nxt.astimezone(pytz.utc)

        nxt = pytz.timezone('Europe/Berlin').normalize(nxt)
        nxt = f'{nxt.year}.{nxt.month:02}.{nxt.day:02} {nxt.hour:02}:{nxt.minute:02}'
        await ctx.send(f'Nächster Putz ist am: {nxt}')
