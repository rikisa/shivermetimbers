"""Lark stuff handling the DEPP BNF definition
"""

from pathlib import Path
from lark import Lark, Token, Tree, LarkError
from lark.visitors import Transformer

import re
from typing import List
from datetime import datetime, timezone as tz, timedelta as td
import pytz

from ..templates import get_template


class _Time:

    _weekdays_long = {
        0: 'Montag',
        1: 'Dienstag',
        2: 'Mittwoch',
        3: 'Donnerstag',
        4: 'Freitag',
        5: 'Samstag',
        6: 'Sonntag'}

    def __init__(self, hh=None, mm=None, DD=None, MM=None, YY=None):

        tz = pytz.timezone('Europe/Berlin')
        self._tz = tz
        now = tz.localize(datetime.now())
        
        update = {}
        for key, val in [('hour', hh), ('minute', mm), ('day', DD),
                ('month', MM), ('year', YY)]:
            try:
                val = int(val)
            except (ValueError, TypeError):
                continue
            update[key] = val

        if update.get('year') is not None and update.get('year') < 100:
            YY_ = (now.year // 100) * 100
            update['year'] = YY_ + update.get('year')

        self.datetime = now.replace(second=0, microsecond=0, **update)

        # if self.datetime < now:
        #     raise ValueError('Can only be in future!')

    def __eq__(self, other):
        if isinstance(other, Clock):
            return self.datetime == other.datetime
        else:
            return self.datetime == other

    def __lt__(self, other):
        if isinstance(other, Clock):
            return self.datetime < other.datetime
        else:
            return self.datetime < other

    def __gt__(self, other):
        if isinstance(other, Clock):
            return self.datetime > other.datetime
        else:
            return self.datetime > other

    def __hash__(self):
        return hash(self.datetime)
    
    @property
    def hour(self):
        return self.datetime.hour

    @property
    def minute(self):
        return self.datetime.minute

    @property
    def day(self):
        return self.datetime.day

    @property
    def month(self):
        return self.datetime.month

    @property
    def year(self):
        return self.datetime.year

    @property
    def weekday(self):
        return self._weekdays_long[self.datetime.weekday()]


class Clock(_Time):

    _clock_pat = re.compile(
        (Path(__file__).parent / 'time.re').open('r').read(),
        re.U | re.I | re.X)

    @classmethod
    def from_str(cls, content: str):
        match = cls._clock_pat.match(content)
        return cls(**match.groupdict())

    def __str__(self):
        return f'{self.hour}:{self.minute:>02d}'


class Date(_Time):

    _date_pat = re.compile(
        (Path(__file__).parent / 'date.re').open('r').read(),
        re.U | re.I | re.X)

    @classmethod
    def from_str(cls, content: str):
        match = cls._date_pat.match(content)
        return cls(**match.groupdict())

    def __str__(self):
        return f'{self.day:>02d}.{self.month:>02d}.'


class TransformerError(LarkError):

    def __init__(self, token: Token):
        self.line = token.line
        self.column = token.column


class PostTransformer(Transformer):

    _depp = Lark.open(Path(__file__).with_suffix('.lark'),
                      start='post', parser='lalr',
                      lexer='contextual', debug=False)

    @classmethod
    def parse_post(cls, post: str) -> dict:
        tree = cls.to_tree(post)
        return cls().transform(tree)

    @classmethod
    def to_tree(cls, post: str) -> Tree:
        return cls._depp.parse(post)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._reactions = {}
        self._infokey_map = {
            'uhr': Clock.from_str,
            'datum': Date.from_str,
        }

        self._extkey_map = {
            'erinnern': _emoji_to_list
        }

    def add_reaction(self, emoji):
        self._reactions[str(emoji)] = self._reactions.get(
            str(emoji), list())

    def assignment(self, tokens):
        infokey, value = [tk.value.strip() for tk in tokens]
        infokey = infokey.lower()
        mapping_func = self._infokey_map.get(infokey, str)

        return {infokey: mapping_func(value)}

    def extension(self, tokens):
        extkey, value = [tk.value.strip() for tk in tokens]
        mapping_func = self._extkey_map.get(extkey, str)

        return {extkey: mapping_func(value)}

    def basics(self, dicts):
        ret = {}
        for dic in dicts:
            ret.update(dic)
        return ret

    def template(self, strings):
        return ''.join(strings)

    def substitute(self, tokens):
        token_value = tokens[0]
        infokey = token_value.value.strip().lower()
        tmpl = f"basics.get('{infokey}', '?!?{token_value}')"
        return '{{' + tmpl + '|e}}'

    def raw_jinja(self, token):
        return token[0].value

    def reaction_count(self, tokens):
        reaction_token = tokens[0].value
        self.add_reaction(reaction_token)
        templ = f"reactions.get('{reaction_token}', []) | length"
        return '{{ ' + templ + ' }}'

    def reaction_names(self, tokens):
        reaction_token = tokens[0].value
        self.add_reaction(reaction_token)
        templ = f"reactions.get('{reaction_token}', []) | join(', ')"
        return '{{ ' + templ + ' }}'

    def empty_macro(self, tokens):
        return self._wrapp_error('!!!')
    
    def invalid_macro(self, tokens):
        tkn = tokens[0]
        if tkn.type == 'UNKNOWN':
            ret = f"??> '{tkn.value}'"
        elif tkn.type == 'EMPTY':
            ret = '!!!'
        else:
            ret = f"THIS IS AWFULLY BROKEN! '{tkn.value}'"

        return self._wrapp_error(ret)

    def _wrapp_error(self, content: str):
        return '{%raw%}_{' + content + '}_{%endraw%}'

    def macro(self, strings):
        """ just pass on strings generated
        by valid macros
        """
        return ''.join(strings)
    
    def add_alternatives(self, basics):
        if 'datum' in basics:
            date = basics['datum']
            basics['w:datum'] = f'{date.weekday}, {str(date)}'
            basics['y:datum'] = f'{str(date)}{date.year}'
            basics['wy:datum'] = f'{date.weekday}, {str(date)}{date.year}'

    def post(self, processed):
        if len(processed) == 2:
            # basics and template
            basics, template = processed
            self.add_alternatives(basics)
        else:
            # if smaller, then the optional
            # basics is left
            template, = processed
            basics = {}

        emojis = list(self._reactions.keys())
        return dict(basics=basics, template=template, emojis=emojis)


def _emoji_to_list(string):
    emojis = string.split(',')
    if emojis[0] == '':
        raise ValueError('Empty list/string')
    return set([em.strip(' \n\r\t\b') for em in emojis])

