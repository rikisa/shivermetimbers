"""Interface between lark stuff and discord bot and does the template rendering
with jinja2
"""
from jinja2 import Template
import re

from depp import PostTransformer


class SessionTime:
    """Date and time parser and object
    """

    _weekdays_short = {
        0: 'Mo.',
        1: 'Di.',
        2: 'Mi.',
        3: 'Do.',
        4: 'Fr.',
        5: 'Sa.',
        6: 'So.'}

    _weekdays_long = {
        0: 'Montag',
        1: 'Dienstag',
        2: 'Mittwoch',
        3: 'Donnerstag',
        4: 'Freitag',
        5: 'Samstag',
        6: 'Sonntag'}

    _full_pat = re.compile(
        '^(?P<pre>.*?)' +
        '(?:(?:' + '|'.join(list(_weekdays_long.values()) +
                            list(_weekdays_short.values())) + '), +)?' +
        '(?P<day>\d{1,2})\.(?P<mon>\d{1,2})\.?' +
        '(?:(?: *, *| *| *\| *| +um +| +ab +)?(?P<hh>\d{1,2}):(?P<mm>\d{1,2}))?'
        + '(?P<post>.*?)$',
        re.IGNORECASE)

    def __init__(self, day, mon, hh=None, mm=None, pre=None, post=None):
        """Create a SessionTime object. Needs at elast day and month (mon)
        """

        tz = pytz.timezone('Europe/Berlin')
        now =tz.localize(datetime.now())

        # dateime object, could be cooler...
        self.datetime = now.replace(day=day, month=mon, hour=0, minute=0,
                                    second=0, microsecond=0)
        if hh is not None and mm is not None:
            hh, mm = [int(v) for v in (hh, mm)]
            self.datetime = self.datetime.replace(hour=hh, minute=mm)
            self.hh = hh
            self.mm = mm
        else:
            self.hh = self.mm = None

        self.day = day
        self.mon = mon

        self.pre = pre
        self.post = post

    @property
    def weekday_long(self):
        return self._weekdays_long[self.datetime.weekday()]

    @property
    def weekday_short(self):
        return self._weekdays_short[self.datetime.weekday()]

    def __str__(self):
        ret = f'{self.weekday_long}, {self.day:0>2}.{self.mon:0>2}.'
        if self.hh is not None:
            ret += f' um {self.hh:0>2}:{self.mm:0>2} Uhr'
        return ret

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if isinstance(other, SessionTime):
            return self.datetime == other.datetime
        else:
            return self.datetime == other

    def __lt__(self, other):
        if isinstance(other, SessionTime):
            return self.datetime < other.datetime
        else:
            return self.datetime < other

    def __gt__(self, other):
        if isinstance(other, SessionTime):
            return self.datetime > other.datetime
        else:
            return self.datetime > other

    def __hash__(self):
        return hash(self.datetime)

    @classmethod
    def from_str(cls, content):
        match = cls._full_pat.match(content)
        if match is None:
            return None
        values = match.groupdict()
        for k in ('day', 'mon', 'hh', 'mm'):
            v = values.get(k)
            if v is not None:
                v = int(v)
            values[k] = v
        values.setdefault('pre', '')
        values.setdefault('post', '')
        try:
            return cls(**values)
        except ValueError:
            return None


# with open('../../BALD/macros.txt', 'r') as src:
#   transformed = PostTransformer.parse_post(src.read())
# 
# reactions = {'B': ['Foo', 'BAR'],
#              'C': ['Otto']}
# print('\n\n')
# template = transformed['template']
# print(template)
# jjt = Template(template)
# print(jjt.render(reactions=reactions, basics=transformed.get('basics', {})))
