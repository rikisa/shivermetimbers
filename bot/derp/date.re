^\s*            # leading ws
(?P<DD>
  \d{1,2}       # one or two digits for day
)

\s*\.\s*        # separated by dot, with maybe ws inbetween

(?P<MM>
  \d{1,2}       # one or two digits for month
)

\s*\.?\s*       # Optional dot

(
(?P<YY>
  (\d{4}|\d{2}) # two or four digits for year
)[^\d]*)?       # year is optional 
                # but must be end with non number
