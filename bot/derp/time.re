^\s*            # leading ws
(?P<hh>
  \d{1,2}       # one or two digits for hours
)

\s*\:\s*        # separated by dot, with maybe ws inbetween

(?P<mm>
  \d{1,2}       # one or two digits for minutes
)
