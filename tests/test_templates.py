from bot.templates import get_template


def test_wremind():
    ought = """_ _
───── **TITEL** ─────
geht los!

_Am Freitag, 13.13.1313, um 00:00 Uhr_

Für  B haben (2) gestimmt: Foo, Bar

_ _"""
        
    jnjtmpl = get_template('reminder')
    msg_content = jnjtmpl.render(
        basics={
            'titel': 'TITEL',
            'w:datum': 'Freitag, 13.13.1313',
            'uhr': '00:00',
            'erinnern': set(['B']),
            },
        reactions={'A': ['Foo'], 'B': ['Foo', 'Bar']})

    assert msg_content == ought

def test_woremind():
    ought = """_ _
───── **TITEL** ─────
geht los!

_Am Freitag, 13.13.1313, um 00:00 Uhr_


_ _"""
        
    jnjtmpl = get_template('reminder')
    msg_content = jnjtmpl.render(
        basics={
            'titel': 'TITEL',
            'w:datum': 'Freitag, 13.13.1313',
            'uhr': '00:00',},
        reactions={'A': ['Foo'], 'B': ['Foo', 'Bar']})

    assert msg_content == ought
