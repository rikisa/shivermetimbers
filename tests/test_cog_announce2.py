import pytest
import discord.ext.test as dpytest
import logging
import discord
import copy

LOG = logging.getLogger('Shiver')
# LOG.addHandler(logging.StreamHandler())
LOG.setLevel(logging.DEBUG)
# LOG.debug('Logging on')

### dpytest
# message -> mock message
# get_message -> get last message from bot
# add_reaction -> add mock reaction

from bot.__main__ import get_bot


TEST_TMPL = """
Titel: Der Ottomane schläf ned
uhr: 13:33
datum: 1.1.1112
kanal: log
<->
{titel}

den emoji 🙉 haben {?🙉} leute angewählt und zwar: {@ 🙉 } 

am { datum} um {uhr} !!!!!!

"""

# from IPython import embed
# import nest_asyncio
# nest_asyncio.apply()


@pytest.fixture
async def ctrl_environ(event_loop):
    bot = get_bot(announce_cid=12345, debug_id=1234)
    dpytest.configure(bot,
        num_guilds=1,
        num_channels=1,
        num_members=2,
    )

    run_conf = dpytest.get_config()
    annman = bot.cogs.get('AnnounceManager')

    guild = run_conf.guilds[0]
    ann_ch = await guild.create_text_channel(name='Announce')
    annman.set_global_announce_cid(ann_ch.id)

    ctrl_ch = await guild.create_text_channel(name=annman._control_channel)
    ctrl_role = await guild.create_role(name=annman._control_role)
    ctrl_usr = await dpytest.member_join(guild, user=None, name='Hussar!')
    await dpytest.add_role(member=ctrl_usr, role=ctrl_role)

    return bot, ctrl_ch, ctrl_usr, ann_ch

@pytest.mark.asyncio
async def test_parsing(ctrl_environ):
    bot, ctrl_ch, ctrl_usr, ann_ch = ctrl_environ
    cog = bot.cogs['AnnounceManager']
    tmpl = await dpytest.message(content=TEST_TMPL,
        channel=ctrl_ch, member=ctrl_usr)
    # tmpl_old = copy.copy(tmpl)
    dpytest.back.pin_message(channel_id=ctrl_ch.id,
            message_id=tmpl.id)
    # await cog.on_message_edit(tmpl_old, tmpl)
    # embed(using='asyncio')

@pytest.mark.asyncio
async def test_version(ctrl_environ):
    bot, _, _, _ = ctrl_environ
    await dpytest.message('$version')
    assert dpytest.verify().message().contains().content("ShiverMeTimbers")
    # embed(using='asyncio')
