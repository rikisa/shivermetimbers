import pytest
from bot.derp.derp import PostTransformer, Date, Clock
from datetime import datetime, timedelta
import pytz
from itertools import product
from jinja2 import Template

from lark import LarkError

import IPython as ip


NEXT = ((pytz.timezone('Europe/Berlin')
         .localize(datetime.now()))
          + timedelta(seconds=3600)
          + timedelta(days=365))
TEST_TMPL = (f"""

Titel: Der Ottomane schläf ned
uhr: {NEXT.hour}:{NEXT.minute}
datum: 01 .2 . {NEXT.year}
kanal: log""" + """

<->

{titel}

den emoji 🙉 haben {#🙉} leute angewählt und zwar: {@ 🙉 } 

am { datum} um {uhr} !!!!!! this one does nothing: 🥰,
this one 😒  not: { # 😒  }
""")
TEST_KEYS = ('titel', 'uhr', 'datum', 'kanal')
TEST_WATCHED_EMOJIS = ('🙉', '😒')


def test_parsed_dict():
    parsed_dict = PostTransformer.parse_post(TEST_TMPL)
    tree = PostTransformer.to_tree(TEST_TMPL)
    print()
    print(tree.pretty('..'))
    assert all(list(kw in parsed_dict['basics'] for kw in TEST_KEYS))
    assert all(list(ke in parsed_dict['emojis'] for ke in \
                    TEST_WATCHED_EMOJIS))

def test_parsed_dict_infokeys():
    parsed_dict = PostTransformer.parse_post(TEST_TMPL)
    bsi = parsed_dict['basics']
    assert bsi['titel'] == 'Der Ottomane schläf ned'
    assert bsi['kanal'] == 'log'
    assert bsi['uhr'].hour == NEXT.hour
    assert bsi['uhr'].minute == NEXT.minute
    assert bsi['datum'].day == 1
    assert bsi['datum'].month == 2
    assert bsi['datum'].year == NEXT.year

# def test_minimal_past():
#     with pytest.raises(ValueError):
#         Date.from_str('1.1.1292')

@pytest.mark.parametrize("msg,expected", [
    ('{tittel}', "_{??> 'tittel'}_"),
    ('{titt tel !}', "_{??> 'titt tel !'}_"),
    ('{}', '_{!!!}_'),
    ('{  }', '_{!!!}_'),
    ])
def test_unknown_macro(msg, expected):
    mini_post = """Titel: otto
    uhr: 23:59
    datum: 1.1.9999
    <->
    """
    # is okay, to allow custom smileys
    # Invalid;{ # AAbb  };!!!
    # Invalid2;{@ AsdAbaaaa-- };!!!
    parsed_dict = PostTransformer.parse_post(mini_post + msg)
    final = Template(parsed_dict['template']).render(
        basics=parsed_dict['basics'], reactions={})

    assert final == expected

@pytest.mark.parametrize("msg", ('{# a b cd}', '{#ab cd}', '{ @ab c d}'))
def test_raising_macro_errors(msg):
    basics = """Titel: otto
    uhr: 23:59
    datum: 1.1.9999
    <->
    """

    with pytest.raises(LarkError):
        parsed_dict = PostTransformer.parse_post(basics + msg)

    # final = Template(parsed_dict['template']).render(
    #     basics=parsed_dict['basics'], reactions={})


def test_jinja2_render():
    mini_post = """Titel: otto
    uhr: 23:59
    datum: 1.1.9999
    <->
    {titel} { datum} {uhr}"""

    parsed_dict = PostTransformer.parse_post(mini_post)
    final = Template(parsed_dict['template']).render(
        basics=parsed_dict['basics'], reactions={})
    
    assert final == 'otto 01.01. 23:59'

def test_render_alts():
    mini_post = """Titel: otto
    uhr: 23:59
    datum: 1.1.9999
    <->
    {datum}\n{y:datum}\n{w:datum}\n{wy:datum}"""

    parsed_dict = PostTransformer.parse_post(mini_post)
    final = Template(parsed_dict['template']).render(
        basics=parsed_dict['basics'], reactions={})
    
    oughts = ['01.01.', '01.01.9999', 'Freitag, 01.01.', 'Freitag, 01.01.9999']
    for isdate, ought in zip(final.split('\n'), oughts):
        assert isdate == ought

def test_no_header():
    header_with_sep = """<->
    Just capture reactions, ya' know?
    """
    parsed_dict = PostTransformer.parse_post(header_with_sep)
    final = Template(parsed_dict['template']).render(
        basics=parsed_dict['basics'], reactions={})
    assert final == header_with_sep

    just_text = """Just capture reactions, ya' know?
    """
    parsed_dict = PostTransformer.parse_post(just_text)
    final = Template(parsed_dict['template']).render(
        basics=parsed_dict['basics'], reactions={})
    assert final == just_text

def test_raw_jinja():
    header_with_sep = """
    titel: foobar
    <->
    Just capture reactions, ya' know? {{ basics.titel }}"""
    parsed_dict = PostTransformer.parse_post(header_with_sep)
    tmpl = parsed_dict['template']
    final = Template(tmpl).render(
        basics=parsed_dict['basics'], reactions={})

    assert tmpl == "Just capture reactions, ya' know? {{ basics.titel }}"
    assert final == "Just capture reactions, ya' know? foobar"

def test_extensions():
    header_with_ext = """
    titel: foobar
    erinnern: A, <:abc:1234>
    <->
    Just capture reactions, ya' know? {{ basics.titel }}"""

    parsed_dict = PostTransformer.parse_post(header_with_ext)
    tmpl = parsed_dict['template']
    basics = parsed_dict['basics']
    final = Template(tmpl).render(
        basics=basics, reactions={})

    assert final == "Just capture reactions, ya' know? foobar"
    assert set(basics['erinnern']) == set(['A', '<:abc:1234>'])
