from random import randint
from bot.commandgroups.announce2 import LinkSignature
import struct
import base64


def test_link_cycle_encrypted():
    ids = tuple(randint(10**17, 10**18) for i in range(3))
    # ids = tuple(randint(10**1, 10**2) for i in range(3))
    l0 = LinkSignature(*ids, encrypt_sig=True)
    l1 = LinkSignature.from_signature(l0.signature)

    assert l0.encrypt
    assert l1.encrypt

    assert (l0.gid, l0.cid, l0.mid) == ids
    assert (l1.gid, l1.cid, l1.mid) == ids
    assert l0.signature == l1.signature

def test_link_cycle_unencrypted():
    ids = tuple(randint(10**17, 10**18) for i in range(3))
    l0 = LinkSignature(*ids)
    
    l1 = LinkSignature.from_signature(l0.signature)

    assert not l0.encrypt
    assert not l1.encrypt

    assert (l0.gid, l0.cid, l0.mid) == ids
    assert (l1.gid, l1.cid, l1.mid) == ids
    assert l0.signature == l1.signature

def test_link_cmp_crypted():
    ids = tuple(randint(10**17, 10**18) for i in range(3))
    l0 = LinkSignature(*ids, encrypt_sig=False)
    l1 = LinkSignature(*ids, encrypt_sig=True)

    assert not l0.encrypt
    assert l1.encrypt

    assert (l0.gid, l0.cid, l0.mid) == ids
    assert (l1.gid, l1.cid, l1.mid) == ids
    assert l0.signature != l1.signature
    
    # dismiss header stuff
    idvals0 = base64.b85decode(l0.signature)[2:]
    idvals1 = base64.b85decode(l1.signature)[2:]

    assert idvals0 != idvals1


def test_link_unencrypted_default():
    ids = (0xbe, 0xef , 0xaa)
    l0 = LinkSignature(*ids)
    
    packed = base64.b85decode(l0.signature)
    encrypt, byte_len, gid, cid, mid = struct.unpack('BBBBB', packed)

    assert (l0.gid, l0.cid, l0.mid) == (gid, cid, mid)
    assert (gid, cid, mid) == ids
    assert not encrypt
    assert byte_len == 1
    assert l0.id_string == '/'.join(str(i) for i in ids)

def test_link_from_string():
    ids = (0xbe, 0xef , 0xaa)
    l0 = LinkSignature(*ids)

    l1 = LinkSignature.from_string('/'.join(str(i) for i in ids))

    assert l0.signature == l1.signature
    

# def test_attack():
# 
#     # ids = tuple(randint(10000, 99999) for i in range(3))
#     ids = (0xefbe, 0xfeca, 0xcdab)
#     l0 = LinkSignature(*ids, encrypt_sig=True)
# 
#     from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
# 
#     decoded = base64.b85decode(l0.signature)
#     iv = decoded[1:9]
#     encyphered = decoded[9:]
#     mode = modes.CFB(iv)
#     max_val = 2**(4*8)
#     
#     def check_range(start, stop):
#         for key_cand in range(start, stop):
#             # print(f'\r{key_cand}/{max_val}: {key_cand/max_val:.2f}', end='')
#             key_cand = key_cand.to_bytes(4, 'little')
#             # key_cand = l0._secret
#             cipher = Cipher(algorithms.Blowfish(key_cand), mode)
#             decr = cipher.decryptor()
#             decyphered = decr.update(encyphered) + decr.finalize()
#             if decyphered[1:] == l0.id_bytes:
#                 # print('hell yeah!')
#                 return key_cand
#         return None
# 
#     from threading import ThreadPool
# 
#     # _iv_bytes = 8
#     # _secret = bytes.fromhex(
#     #     os.environ.get('SHIVER_LINK_NONCE', os.urandom(4).hex()))
